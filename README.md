# Opdracht 1 
Advies: 10min

Open OpstellingControllerTest. Als je de tests draait zie je dat er twee falen. Zorg dat alle tests in deze class slagen.


# Opdracht 2 
Advies: 10min

Bekijk het /treinen endpoint (POST en GET) en beschrijf hier onder de tests op die je hier voor wilt doen om deze code veilig op productie te kunnen 
deployen.

De tests die ik zou toevoegen zijn:
*
*
*


# Opdracht 3
Advies: 25min

In deze opdracht moet er een feature gebouwd worden die alle mogelijke treincombinaties (Opstellingen) teruggeeft die passen op een spoor. Een spoor
heeft een maximaal aantal wagons dat erop passen, en een trein bestaat uit een aantal wagons. 

De requirements voor deze feature zijn:
* Alle unieke treincombinaties moeten in het resultaat staan.
* Volgorde is niet belangrijk: "IC, ICE" en "ICE, IC" zijn bijvoorbeeld gelijk. Dit moet ontdubbeld worden.
* Een trein kan meerdere malen op een spoor gezet worden. Bijvoorbeeld: gegeven een IC met lengte 2 op een spoor met lengte 6 is "IC, IC, IC" een 
  mogelijke uitkomst.
* Treinen met minder dan 1 wagons mogen niet in het resultaat terechtkomen.
* De applicatie moet om kunnen gaan met een lege set treinen.

De TreinOphaalControllerIntegratieTest bevat de meest belangrijke requirements, begin hiermee. TreinOpstelCombinatiesTest test de overige requirements 
die je daarna kan oppakken.

Gezien de gelimiteerde tijd is het niet erg als je niet alle testen groen kunt krijgen.


# Slot
De laatste 10min praten we over de implementatie.
