package nl.donna.treinenplanner;

import nl.donna.treinenplanner.repository.StationRepository;
import nl.donna.treinenplanner.repository.TreinRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class OpstellingControllerTest {
     @Autowired
     private OpstellingController controller;

     @Autowired
     private TreinRepository treinRepository;

     @Autowired
     private StationRepository stationRepository;

     @Test
     void shouldReturnAmountOfTrainsThatFit() {
          // GIVEN a train with size 3, a train with size 2 and a station with a track of size 8
          treinRepository.setTreinen(List.of(new Trein("ICE", 3), new Trein("IC", 2)));
          stationRepository.setStations(Map.of("Ut", List.of(new Spoor("1A", 8))));

          // WHEN I query the track capacity for IC trains
          final Integer result = controller.getMaximaalAantalTreinenOpSpoor("Ut", "1A", "IC");

          // THEN the IC should fit four times
          assertThat(result).isEqualTo(4);
     }

     @Test
     void shouldReturnAmountTrainsRoundedDown() {
          // GIVEN a train with size 3 and a station with a track of size 8
          treinRepository.setTreinen(List.of(new Trein("MAT64", 3)));
          stationRepository.setStations(Map.of("Ut", List.of(new Spoor("1A", 8))));

          // WHEN I query the track capacity for MAT64 trains
          final Integer result = controller.getMaximaalAantalTreinenOpSpoor("Ut", "1A", "MAT64");

          // THEN the MAT64 should fit two times
          assertThat(result).isEqualTo(2);
     }

     @Test
     void shouldReturnZeroWhenStationNotFound() {
          // GIVEN a train and no stations
          treinRepository.setTreinen(List.of(new Trein("MAT64", 3)));
          stationRepository.setStations(Collections.emptyMap());

          // WHEN I query the track capacity for MAT64 trains
          final Integer result = controller.getMaximaalAantalTreinenOpSpoor("Ut", "1A", "MAT64");

          // THEN the result defaults to 0
          assertThat(result).isZero();
     }

     @Test
     void shouldReturnZeroWhenThereAreNoTrains() {
          // GIVEN no trains and a station
          treinRepository.setTreinen(Collections.emptyList());
          stationRepository.setStations(Map.of("Ut", List.of(new Spoor("1A", 8))));

          // WHEN I query the track capacity for MAT64 trains
          final Integer result = controller.getMaximaalAantalTreinenOpSpoor("Ut", "1A", "MAT64");

          // THEN the result defaults to 0
          assertThat(result).isZero();
     }

     @Test
     void shouldReturnZeroWhenTrainNotFound() {
          // GIVEN a train and a station
          treinRepository.setTreinen(List.of(new Trein("ICE", 3)));
          stationRepository.setStations(Map.of("Ut", List.of(new Spoor("1A", 8))));

          // WHEN I query the track capacity for an unknown train
          final Integer result = controller.getMaximaalAantalTreinenOpSpoor("Ut", "1A", "DOES_NOT_EXIST");

          // THEN the result defaults to 0
          assertThat(result).isZero();
     }
}
