package nl.donna.treinenplanner;

import nl.donna.treinenplanner.repository.StationRepository;
import nl.donna.treinenplanner.repository.TreinRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.List.of;
import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest
class TreinOphaalControllerIntegratieTest {
     @Autowired
     private TreinOphaalController controller;

     @Autowired
     private TreinRepository treinRepository;

     @Autowired
     private StationRepository stationRepository;

     @Test
     void shouldReturnOpstelCapaciteitMultipleTrains() {
          // GIVEN trains are present
          treinRepository.setTreinen(of(new Trein("IC", 4), new Trein("ICE", 6), new Trein("MAT64", 3), new Trein("SPR", 7)));
          // AND station AMF is present
          stationRepository.setStations(Map.of("Amf", List.of(new Spoor("1a", 4), new Spoor("4", 10))));

          // WHEN the service is queried
          final List<String> result = getOpstelCombinaties();

          // THEN all valid combinations should be in the response
          assertThat(result)
                  .contains("IC,ICE", "ICE,MAT64", "IC,MAT64", "MAT64,SPR", "IC,IC", "MAT64,MAT64");
     }

     @Test
     void shouldReturnOpstelCapaciteitSingleTrains() {
          // GIVEN trains are present
          treinRepository.setTreinen(of(new Trein("IC", 4), new Trein("ICE", 6), new Trein("MAT64", 3), new Trein("SPR", 7)));
          // AND station AMF is present
          stationRepository.setStations(Map.of("Amf", List.of(new Spoor("1a", 4), new Spoor("4", 10))));

          // WHEN the service is queried
          final List<String> result = getOpstelCombinaties();

          // THEN all single trains should be in the response
          assertThat(result)
                  .contains("IC", "ICE", "MAT64", "SPR");
     }

     private List<String> getOpstelCombinaties() {
          return controller.getOpstelCombinaties("Amf", "4").stream()
                  .map(opstelling -> opstelling.getTreinen().stream().map(Trein::getNaam).sorted().collect(Collectors.joining(",")))
                  .collect(Collectors.toList());
     }
}
