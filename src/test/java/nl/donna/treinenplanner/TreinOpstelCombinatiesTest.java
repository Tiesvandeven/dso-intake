package nl.donna.treinenplanner;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class TreinOpstelCombinatiesTest {

     @Test
     void testHappyFlow() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("ICE1", 1), new Trein("ICE2", 2), new Trein("ICE3", 3));
          List<Opstelling> opstelCombinaties = TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor);
          List<String> aantalWagonCombinaties = opstelCombinaties.stream()
                  .map(it -> it.getTreinen().stream()
                          .map(Trein::getNaam)
                          .sorted()
                          .collect(Collectors.joining(",")))
                  .collect(Collectors.toList());
          assertThat(aantalWagonCombinaties)
                  .containsExactlyInAnyOrder(
                          "ICE1",
                          "ICE1,ICE1",
                          "ICE1,ICE1,ICE1",
                          "ICE1,ICE1,ICE1,ICE1",
                          "ICE1,ICE1,ICE1,ICE1,ICE1",
                          "ICE1,ICE2",
                          "ICE1,ICE2,ICE2",
                          "ICE1,ICE1,ICE2",
                          "ICE1,ICE1,ICE1,ICE2",
                          "ICE1,ICE3",
                          "ICE1,ICE1,ICE3",
                          "ICE2",
                          "ICE2,ICE2",
                          "ICE2,ICE3",
                          "ICE3"
                  );
     }

     @Test
     void testResultDoesNotExceedTrackCapacity() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("ICE1", 1), new Trein("ICE2", 2), new Trein("ICE3", 3));
          List<Opstelling> opstelCombinaties = TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor);
          List<Integer> combinatieLengtes = opstelCombinaties.stream()
                  .map(it -> it.getTreinen().stream()
                          .mapToInt(Trein::getAantalWagons)
                          .sum())
                  .collect(Collectors.toList());

          assertThat(combinatieLengtes).isNotEmpty().allMatch(integer -> integer <= spoor.wagonCapaciteit());
     }

     @Test
     void shouldGiveAllCombinationsOfTwoTrainTypes() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("ICE1", 3), new Trein("ICE2", 2), new Trein("ICE3", 1));
          List<Opstelling> opstelCombinaties = TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor);
          List<String> aantalWagonCombinaties = opstelCombinaties.stream()
                  .map(it -> it.getTreinen().stream()
                          .map(Trein::getNaam)
                          .sorted()
                          .collect(Collectors.joining(",")))
                  .collect(Collectors.toList());
          assertThat(aantalWagonCombinaties)
                  .containsExactlyInAnyOrder(
                          "ICE3",
                          "ICE3,ICE3",
                          "ICE3,ICE3,ICE3",
                          "ICE3,ICE3,ICE3,ICE3",
                          "ICE3,ICE3,ICE3,ICE3,ICE3",
                          "ICE2",
                          "ICE2,ICE2",
                          "ICE1",
                          "ICE2,ICE3",
                          "ICE2,ICE2,ICE3",
                          "ICE2,ICE3,ICE3",
                          "ICE2,ICE3,ICE3,ICE3",
                          "ICE1,ICE3",
                          "ICE1,ICE3,ICE3",
                          "ICE1,ICE2"
                  );
     }

     @Test
     void shouldGiveAllCombinationsOfThreeTrainTypes() {
          Spoor spoor = new Spoor("", 6);
          List<Trein> treinen = List.of(new Trein("ICE1", 3), new Trein("ICE2", 2), new Trein("ICE3", 1));
          List<Opstelling> opstelCombinaties = TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor);
          List<String> aantalWagonCombinaties = opstelCombinaties.stream()
                  .map(it -> it.getTreinen().stream()
                          .map(Trein::getNaam)
                          .sorted()
                          .collect(Collectors.joining(",")))
                  .collect(Collectors.toList());
          assertThat(aantalWagonCombinaties)
                  .containsExactlyInAnyOrder(
                          "ICE1",
                          "ICE1,ICE1",
                          "ICE1,ICE2",
                          "ICE1,ICE2,ICE3",
                          "ICE1,ICE3",
                          "ICE1,ICE3,ICE3",
                          "ICE1,ICE3,ICE3,ICE3",
                          "ICE2",
                          "ICE2,ICE2",
                          "ICE2,ICE2,ICE2",
                          "ICE2,ICE2,ICE3",
                          "ICE2,ICE2,ICE3,ICE3",
                          "ICE2,ICE3",
                          "ICE2,ICE3,ICE3",
                          "ICE2,ICE3,ICE3,ICE3",
                          "ICE2,ICE3,ICE3,ICE3,ICE3",
                          "ICE3",
                          "ICE3,ICE3",
                          "ICE3,ICE3,ICE3",
                          "ICE3,ICE3,ICE3,ICE3",
                          "ICE3,ICE3,ICE3,ICE3,ICE3",
                          "ICE3,ICE3,ICE3,ICE3,ICE3,ICE3"
                  );
     }

     @Test
     void testDoubles() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("ICE1", 2), new Trein("ICE2", 2));
          List<Opstelling> opstelCombinaties = TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor);
          List<String> aantalWagonCombinaties = opstelCombinaties.stream()
                  .map(it -> it.getTreinen().stream()
                          .map(Trein::getNaam)
                          .sorted()
                          .collect(Collectors.joining(",")))
                  .collect(Collectors.toList());
          assertThat(aantalWagonCombinaties)
                  .hasSize(5)
                  .contains("ICE1")
                  .contains("ICE1,ICE1")
                  .contains("ICE1,ICE2")
                  .contains("ICE2")
                  .contains("ICE2,ICE2");
     }

     @Test
     void testNoTrains() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of();
          assertThat(TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor)).isEmpty();
     }

     @Test
     void testTooLargeTrains() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("", 6));
          assertThat(TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor)).isEmpty();
     }

     @Test
     void testTooSmallTrainsEmptyResult() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("", 0));
          assertThat(TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor)).isEmpty();
     }

     @Test
     void testTooSmallTrainsNotInResult() {
          Spoor spoor = new Spoor("", 5);
          List<Trein> treinen = List.of(new Trein("", 0), new Trein("", 5));
          assertThat(TreinOpstelCombinaties.getOpstelCombinaties(treinen, spoor)).hasSize(1);
     }
}
