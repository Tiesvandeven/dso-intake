package nl.donna.treinenplanner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TreinOpstelCombinaties {
     private TreinOpstelCombinaties() {
          // Only static methods, no need to instantiate
     }

     /**
      * Opdracht 3
      *
      * In deze opdracht moet er een feature gebouwd worden die alle mogelijke treincombinaties (Opstellingen) teruggeeft die passen op een spoor.
      * Een spoor heeft een maximaal aantal wagons dat erop passen, en een trein bestaat uit een aantal wagons.
      *
      * De requirements voor deze feature zijn:
      * - Alle unieke treincombinaties moeten in het resultaat staan.
      * - Volgorde is niet belangrijk: "IC, ICE" en "ICE, IC" zijn bijvoorbeeld gelijk. Dit moet ontdubbeld worden.
      * - Een trein kan meerdere malen op een spoor gezet worden. Bijvoorbeeld: gegeven een IC met lengte 2 op een spoor met lengte 6 is "IC, IC, IC"
      *   een mogelijke uitkomst
      * - Treinen met minder dan 1 wagons mogen niet in het resultaat terechtkomen
      * - De applicatie moet om kunnen gaan met een lege set treinen
      */
     public static List<Opstelling> getOpstelCombinaties(List<Trein> treinen, Spoor spoor) {
          return Collections.emptyList();
     }
}
