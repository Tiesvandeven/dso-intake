package nl.donna.treinenplanner;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class TreinOphaalController {
     private final TreinService treinService;

     public TreinOphaalController(final TreinService treinService) {
          this.treinService = treinService;
     }

     @GetMapping("/treinen")
     public List<TreinVO> getTreinen(@RequestParam Optional<String> name) {
          return name.map(treinService::getTreinenMetNaam)
                  .orElseGet(treinService::haalAlleTreinenOp)
                  .stream().map(this::toAPIObject).collect(Collectors.toList());
     }

     private TreinVO toAPIObject(final Trein trein) {
          return new TreinVO(trein.getNaam());
     }

     @GetMapping("/opstelCombinaties")
     public List<Opstelling> getOpstelCombinaties(@RequestParam final String stationsNaam, @RequestParam final String spoorNaam) {
          return treinService.getOpstelCombinaties(stationsNaam, spoorNaam);
     }
}
