package nl.donna.treinenplanner;

import java.util.List;
import java.util.Objects;

public class Opstelling {

    private final List<Trein> treinen;

    public Opstelling(List<Trein> treinen){
        this.treinen = treinen;
    }

    public List<Trein> getTreinen() {
        return treinen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Opstelling that = (Opstelling) o;
        return Objects.equals(treinen, that.treinen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(treinen);
    }

    @Override
    public String toString() {
        return "Opstelling{" +
                "treinen=" + treinen +
                '}';
    }
}
