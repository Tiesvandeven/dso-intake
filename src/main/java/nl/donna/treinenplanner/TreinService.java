package nl.donna.treinenplanner;

import nl.donna.treinenplanner.repository.StationRepository;
import nl.donna.treinenplanner.repository.TreinRepository;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@Validated
public class TreinService {
     private final TreinRepository treinRepository;
     private final StationRepository stationRepository;

     public TreinService(final TreinRepository repository, final StationRepository stationRepository) {
          this.treinRepository = repository;
          this.stationRepository = stationRepository;
     }

     public List<Trein> haalAlleTreinenOp() {
          return treinRepository.getAllTrains();
     }

     public List<Trein> getTreinenMetNaam(final String name) {
          return treinRepository.queryByNameStartsWith(name);
     }

     public void slaTreinOp(@Valid final Trein trein) {
          treinRepository.store(trein);
     }

     public List<Opstelling> getOpstelCombinaties(final String stationsNaam, final String spoorNaam) {
          //Handel side effects af
          final List<Trein> alleTreinen = treinRepository.getAllTrains();
          final Map<String, List<Spoor>> allStations = stationRepository.getAllStations();

          return allStations.get(stationsNaam).stream()
                  .filter(spoor -> spoor.naam().equals(spoorNaam))
                  .findFirst()
                  .map(spoor -> TreinOpstelCombinaties.getOpstelCombinaties(alleTreinen, spoor))
                  .orElseGet(Collections::emptyList);
     }

     /**
      * Opdracht 1
      *
      * Geeft het aantal keer terug dat een trein op een gegeven station geparkeerd staat
      * E.G: een trein met aantalWagons 2 past 3x op een spoor met wagonCapaciteit 7
      * @param stationsNaam
      * @param spoorNaam
      * @param treinNaam
      * @return
      */
     public Integer getAantalOpstellingen(final String stationsNaam, final String spoorNaam, final String treinNaam) {
          return 0;
     }
}
