package nl.donna.treinenplanner;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class Trein {

    private final String naam;
    private final Integer aantalWagons;

    public Trein(@NotBlank(message = "Trein naam mag niet leeg zijn.") String naam, Integer aantalWagons){
        this.naam = naam;
        this.aantalWagons = aantalWagons;
    }

    public String getNaam() {
        return naam;
    }

    public Integer getAantalWagons() {
        return aantalWagons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trein trein = (Trein) o;
        return Objects.equals(naam, trein.naam) && Objects.equals(aantalWagons, trein.aantalWagons);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam, aantalWagons);
    }

    @Override
    public String toString() {
        return "Trein{" +
                "naam='" + naam + '\'' +
                ", aantalWagons=" + aantalWagons +
                '}';
    }
}
