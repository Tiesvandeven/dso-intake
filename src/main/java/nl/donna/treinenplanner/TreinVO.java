package nl.donna.treinenplanner;

import java.util.Objects;

public final class TreinVO {
    private final String naam;

    public TreinVO(String naam) {
        this.naam = naam;
    }

    public String naam() {
        return naam;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (TreinVO) obj;
        return Objects.equals(this.naam, that.naam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam);
    }

    @Override
    public String toString() {
        return "TreinVO[" +
                "naam=" + naam + ']';
    }

}
