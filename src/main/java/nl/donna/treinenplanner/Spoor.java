package nl.donna.treinenplanner;

import java.util.Objects;

public final class Spoor {
    private final String naam;
    private final Integer wagonCapaciteit;

    public Spoor(String naam, Integer wagonCapaciteit) {
        this.naam = naam;
        this.wagonCapaciteit = wagonCapaciteit;
    }

    public String naam() {
        return naam;
    }

    public Integer wagonCapaciteit() {
        return wagonCapaciteit;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Spoor) obj;
        return Objects.equals(this.naam, that.naam) &&
                Objects.equals(this.wagonCapaciteit, that.wagonCapaciteit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam, wagonCapaciteit);
    }

    @Override
    public String toString() {
        return "Spoor[" +
                "naam=" + naam + ", " +
                "wagonCapaciteit=" + wagonCapaciteit + ']';
    }

}
